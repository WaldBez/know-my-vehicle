package waldbezuidenhoutsoftwaresolutions.knowmyvehicle;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;

public class LoadingScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading_screen);

        startTimer();
    }

    private void startTimer() {
        new CountDownTimer(2500, 1000) {
            @Override
            public void onTick(final long millisUntilFinished) {}

            @Override
            public void onFinish() {
                final Intent main_activity = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(main_activity);
            }
        }.start();
    }
}
