package layout;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import waldbezuidenhoutsoftwaresolutions.knowmyvehicle.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class VehiclesDetails extends Fragment {


    public VehiclesDetails() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_vehicles_details, container, false);
    }

}
