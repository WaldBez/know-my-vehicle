package layout;


import android.app.Fragment;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import waldbezuidenhoutsoftwaresolutions.knowmyvehicle.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ManageVehicles extends Fragment {


    public ManageVehicles() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View thisView = inflater.inflate(R.layout.fragment_manage_vehicles, container, false);
        final CardView addVehicleView = (CardView) thisView.findViewById(R.id.add_new_vehicle_view);
        addVehicleView.setVisibility(View.INVISIBLE);
        final FloatingActionButton fabAdd = (FloatingActionButton) thisView.findViewById(R.id.add_new_vehicle);
        setAddFabFunction(addVehicleView, fabAdd);
        return thisView;
    }

    private void setAddFabFunction(final CardView addVehicleView, final FloatingActionButton fabAdd) {
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addVehicleView.setVisibility(View.VISIBLE);
                final int[] height = {addVehicleView.getHeight()};

                new CountDownTimer(2500, 10) {
                    @Override
                    public void onTick(final long millisUntilFinished) {
                        addVehicleView.setLayoutParams(
                                new RelativeLayout.LayoutParams(
                                        ViewGroup.LayoutParams.MATCH_PARENT,
                                        addVehicleView.getLayoutParams().height + 20));
                    }

                    @Override
                    public void onFinish() {}
                }.start();
            }
        });
    }

}
